<?php

$a = array();
$a[0]=1;
$a[1]=3;
$a[2]=1;
$a[3]=4;
$a[4]=2;
$a[5]=3;
$a[6]=5;
$a[7]=4;

echo solution(5, $a, count($a));

function solution($x, $a, $n) {
    $temp_arr = array();
    $exit_sec = 0;

    for($i=0; $i<$n; $i++) {
        if (($a[$i] <= $x) && (in_array($a[$i], $temp_arr) === false)) {
            array_push($temp_arr, $a[$i]);
            $exit_sec = $i;
        }

        if (count($temp_arr) == $x) {
            return $exit_sec;
        }
    }
    return 1;
}