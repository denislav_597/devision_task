<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

function check_triangle($p, $q, $r) {
    //echo "check for: {$p} {$q} {$r}<br>";
    if (($p + $q > $r) && ($q + $r > $p) && ($r + $p > $q)) {
        return 1;
    }
    return 0;
}

function solution($a, $n) {
    for($i=0; $i<$n; $i++) {
        $p = $a[$i];

        for($j=0; $j<$n-1; $j++) {
            $q = $a[$j];

            for($k=0; $k<$j-1; $k++) {
                if (($i!=$j) && ($j !=$k) && ($i != $k))
                    if(check_triangle($p, $q, $a[$k]) == 1)
                        return 1;
            }
        }
    }
    return 0;
}

function solution_two($a, $n) {
    for($i=0; $i<$n; $i++) {
        for($j=0; $j<$n-$i-1; $j++) {
            if ($a[$j] > $a[$j+1]) {
                $c = $a[$j];
                $a[$j] = $a[$j+1];
                $a[$j+1] = $c;
            }
        }
    }

    for($i=0; $i<$n; $i++) {
        if (isset($a[$i+2]) && (check_triangle($a[$i], $a[$i+1], $a[$i+2]) == 1)) {
            return 1;
        }
    }
    return 0;
}

$a = array();
$a[0] = 10;
$a[1] = 2;
$a[2] = 5;
$a[3] = 1;
$a[4] = 8;
$a[5] = 20;


echo solution($a, count($a));
//echo solution_two($a, count($a));




