<?php

$a[0] = 3;
$a[1] = 1;
$a[2] = 2;
$a[3] = 4;
$a[4] = 3;

echo solution($a, count($a));

function solution($a, $n) {
    $min_difference = array_sum($a);
    for($i=0; $i<$n; $i++) {
        $new_difference = calc_difference($a, $i, $n);
        if ($new_difference < $min_difference) {
            $min_difference = $new_difference;
        }
    }
    return $min_difference;
}

function calc_difference($a, $i, $c) {
    $first_part = 0;
    $sec_part = 0;

    for($k=0; $k<$c; $k++) {
        ($i < $k) ? $first_part += $a[$k] : $sec_part += $a[$k];
    }

    return abs($first_part-$sec_part);
}