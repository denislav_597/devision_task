<?php

echo solution(10, 85, 30);

function solution($x, $y, $d) {
    $c=0;
    while($x < $y) {
        $x+=$d;
        $c++;
    }
    return $c;
}