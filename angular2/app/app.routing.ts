import { RouterModule, Routes } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { LandsComponent } from "./lands/lands.component";
import { LandListComponent } from "./lands/land-list/land-list.component";
import { LandEditComponent } from "./lands/land-edit/land-edit.component";
import { LandCreateComponent } from "./lands/land-create/land-create.component";
import { TractorsComponent } from "./tractors/tractors.component";
import { TractorListComponent } from "./tractors/tractor-list/tractor-list.component";
import { TractorCreateComponent } from "./tractors/tractor-create/tractor-create.component";
import { TractorEditComponent } from "./tractors/tractor-edit/tractor-edit.component";
import { CultivatedLandComponent } from "./cultivated-land/cultivated-land.component";
import { CultivatedLandListComponent } from "./cultivated-land/cultivated-land-list/cultivated-land-list.component";
import { CultivatedLandCreateComponent } from "./cultivated-land/cultivated-land-create/cultivated-land-create.component";
import { CultivatedLandEditComponent } from "./cultivated-land/cultivated-land-edit/cultivated-land-edit.component";
import { ReportComponent } from "./report/report.component";
import { LoginComponent } from "./login/login.component";

export const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    },
    /*{
        path: '**',
        redirectTo: '/login',
        pathMatch: 'full'
    },*/
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'lands',
        component: LandsComponent,
        children: [
            {
                path: '',
                component: LandListComponent
            },
            {
                path: 'create',
                component: LandCreateComponent
            },
            {
                path: 'edit/:id',
                component: LandEditComponent
            }
        ]
    },
    {
        path: 'tractors',
        component: TractorsComponent,
        children: [
            {
                path: '',
                component: TractorListComponent
            },
            {
                path: 'create',
                component: TractorCreateComponent
            },
            {
                path: 'edit/:id',
                component: TractorEditComponent
            }
        ]
    },
    {
        path: 'cultivated-land',
        component: CultivatedLandComponent,
        children: [
            {
                path: '',
                component: CultivatedLandListComponent
            },
            {
                path: 'create',
                component: CultivatedLandCreateComponent
            },
            {
                path: 'edit/:id',
                component: CultivatedLandEditComponent
            }
        ]
    },
    {
        path: 'report',
        component: ReportComponent
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);