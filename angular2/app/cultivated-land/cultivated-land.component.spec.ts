import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CultivatedLandComponent } from './cultivated-land.component';

describe('CultivatedLandComponent', () => {
  let component: CultivatedLandComponent;
  let fixture: ComponentFixture<CultivatedLandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CultivatedLandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CultivatedLandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
