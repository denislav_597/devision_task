import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CultivatedLandEditComponent } from './cultivated-land-edit.component';

describe('CultivatedLandEditComponent', () => {
  let component: CultivatedLandEditComponent;
  let fixture: ComponentFixture<CultivatedLandEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CultivatedLandEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CultivatedLandEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
