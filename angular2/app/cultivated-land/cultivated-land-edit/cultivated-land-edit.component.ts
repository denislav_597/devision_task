import { Component, OnInit } from '@angular/core';
import { CultivatedLand } from '../../shared/models/cultivated-land.model';
import { Tractor } from '../../shared/models/tractor.model';
import { Land } from '../../shared/models/land.model';
import { CultivatedLandService } from '../../shared/services/cultivated-land.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TractorService } from '../../shared/services/tractor.service';
import { LandService } from '../../shared/services/land.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-cultivated-land-edit',
  templateUrl: './cultivated-land-edit.component.html',
  styleUrls: ['./cultivated-land-edit.component.css']
})
export class CultivatedLandEditComponent implements OnInit {
  cultivated_land: CultivatedLand;
  tractors: Tractor[];
  lands: Land[];
  errorMsg: string = '';
  cultivatedLandForm = new FormGroup({
    tractor_id: new FormControl(null, Validators.required),
    land_id: new FormControl(null, Validators.required),
    area: new FormControl(null, Validators.required),
    date: new FormControl(null, Validators.required)
  });

  constructor(private service: CultivatedLandService, private route: ActivatedRoute, private router: Router, private serviceTractor: TractorService, private serviceLand: LandService) { }

  ngOnInit() {
    let id = this.route.snapshot.params['id'];
    this.service.getCultivatedLand(id)
      .subscribe(cultivated_land => this.cultivated_land = cultivated_land);

      this.getTractors();
      this.getLands();
  }

  // Get tractors
  getTractors() {
    this.serviceTractor.getTractors()
    .subscribe(tractors => this.tractors = tractors);
  }

  // Get lands
  getLands() {
    this.serviceLand.getLands()
    .subscribe(lands => this.lands = lands);
  }

  updateCultivatedLand() {
    this.errorMsg = '';

    this.service.updateCultivatedLand(this.cultivated_land)
      .subscribe(
        res => {
          if (Number(res) == 0) {
            this.router.navigate(['/cultivated-land']);
          } else {
            this.errorMsg = "Error while editing...";
          }
        },
        err => {
          this.errorMsg = "Error while editing...";
        }
      )
  }

  deleteCultivatedLand(id: number) {
    this.errorMsg = '';

    this.service.deleteCultivatedLand(id)
      .subscribe(res => {
        if (res == 0) {
          this.router.navigate(['/cultivated-land']);
        } else {
          this.errorMsg = "Error while deleting...";
        }
      });
  }

  cancelEdit() {
    this.router.navigate(['/cultivated-land']);
  }

}
