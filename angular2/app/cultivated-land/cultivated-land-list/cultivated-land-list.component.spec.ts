import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CultivatedLandListComponent } from './cultivated-land-list.component';

describe('CultivatedLandListComponent', () => {
  let component: CultivatedLandListComponent;
  let fixture: ComponentFixture<CultivatedLandListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CultivatedLandListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CultivatedLandListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
