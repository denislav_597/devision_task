import { Component, OnInit } from '@angular/core';
import { CultivatedLand } from '../../shared/models/cultivated-land.model';
import { Tractor } from '../../shared/models/tractor.model';
import { Land } from '../../shared/models/land.model';
import { CultivatedLandService } from "../../shared/services/cultivated-land.service";
import { TractorService } from '../../shared/services/tractor.service';
import { LandService } from '../../shared/services/land.service';

@Component({
  selector: 'app-cultivated-land-list',
  templateUrl: './cultivated-land-list.component.html',
  styleUrls: ['./cultivated-land-list.component.css']
})
export class CultivatedLandListComponent implements OnInit {
  cultivated_lands: CultivatedLand[];
  tractors: Tractor[];
  lands: Land[];
  
  errorMsg: string = '';

  constructor(private service: CultivatedLandService, private serviceTractor: TractorService, private serviceLand: LandService) { }

  // Get cultivated lands
  ngOnInit() {
    this.service.getCultivatedLands()
      .subscribe(cultivated_lands => this.cultivated_lands = cultivated_lands);

      this.getTractors();
      this.getLands();
  }

  // Get tractors
  getTractors() {
    this.serviceTractor.getTractors()
    .subscribe(tractors => this.tractors = tractors);
  }

  // Get lands
  getLands() {
    this.serviceLand.getLands()
    .subscribe(lands => this.lands = lands);
  }

  // Delete tractor
  deleteCultivatedLand(id: number) {
    this.errorMsg = '';
    
    this.service.deleteCultivatedLand(id)
      .subscribe(res => {
        if (res == 0) {
          this.ngOnInit();
        } else {
          this.errorMsg = "Error while deleting...";
        }
      });
  }
}
