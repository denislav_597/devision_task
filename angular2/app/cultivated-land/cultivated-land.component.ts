import { Component, OnInit } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cultivated-land',
  templateUrl: './cultivated-land.component.html',
  styleUrls: ['./cultivated-land.component.css']
})
export class CultivatedLandComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    this.checkLoggedIn();
  }

  checkLoggedIn() {
    let loggedin = Cookie.get('loggedin');
    if (Number(loggedin) != 1) {
      this.router.navigate(['/login']);
    }
  }

}
