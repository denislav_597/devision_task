import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CultivatedLandCreateComponent } from './cultivated-land-create.component';

describe('CultivatedLandCreateComponent', () => {
  let component: CultivatedLandCreateComponent;
  let fixture: ComponentFixture<CultivatedLandCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CultivatedLandCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CultivatedLandCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
