import { Component, OnInit } from '@angular/core';
import { CultivatedLand } from '../../shared/models/cultivated-land.model';
import { CultivatedLandService } from '../../shared/services/cultivated-land.service';
import { Router } from '@angular/router';
import { TractorService } from '../../shared/services/tractor.service';
import { LandService } from '../../shared/services/land.service';
import { Tractor } from '../../shared/models/tractor.model';
import { Land } from '../../shared/models/land.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-cultivated-land-create',
  templateUrl: './cultivated-land-create.component.html',
  styleUrls: ['./cultivated-land-create.component.css']
})

export class CultivatedLandCreateComponent implements OnInit {
  cultivated_land: CultivatedLand = { tractor_id: 0, land_id: 0, date: '' };
  tractors: Tractor[];
  lands: Land[];
  errorMsg: string = '';
  cultivatedLandForm = new FormGroup({
    tractor_id: new FormControl(null, Validators.required),
    land_id: new FormControl(null, Validators.required),
    area: new FormControl(null, Validators.required),
    date: new FormControl(null, Validators.required)
  });

  constructor(private service: CultivatedLandService, private router: Router, private serviceTractor: TractorService, private serviceLand: LandService) { }

  ngOnInit() {
    this.getTractors();
    this.getLands();    
  }

  // Get tractors
  getTractors() {
    this.serviceTractor.getTractors()
    .subscribe(tractors => this.tractors = tractors);
  }

  // Get lands
  getLands() {
    this.serviceLand.getLands()
    .subscribe(lands => this.lands = lands);
  }


  createCultivatedLand() {
    this.errorMsg = '';

    this.service.createCultivatedLand(this.cultivated_land)
      .subscribe(res => {
        if (Number(res) == 0) {
          this.router.navigate(['/cultivated-land']);
        } else {
          this.errorMsg = "Error while creating...";
        }
      })
  }

  cancelCreate() {
    this.router.navigate(['/cultivated-land']);
  }

}
