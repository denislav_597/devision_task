import { Component, OnInit } from '@angular/core';
//import { Report } from '../shared/models/report.model';
import { ReportService } from '../shared/services/report.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router } from '@angular/router';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  reports:any;
  constructor(private service: ReportService, private router: Router) { }

   // Get cultivated lands
   ngOnInit() {
    this.checkLoggedIn();

    this.service.getReport()
      .subscribe(data => this.reports = data);
  }

  checkLoggedIn() {
    let loggedin = Cookie.get('loggedin');
    if (Number(loggedin) != 1) {
      this.router.navigate(['/login']);
    }
  }

}
