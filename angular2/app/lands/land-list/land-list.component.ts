import { Component, OnInit } from '@angular/core';
import { Land } from '../../shared/models/land.model';
import { LandService } from '../../shared/services/land.service';

@Component({
  selector: 'app-land-list',
  templateUrl: './land-list.component.html',
  styleUrls: ['./land-list.component.css'],
})

export class LandListComponent implements OnInit {
  lands: Land[];
  errorMsg: string = '';

  constructor(private service: LandService) { }

  // Get lands
  ngOnInit() {
    this.service.getLands()
      .subscribe(lands => this.lands = lands);
  }

  // Delete land
  deleteLand(id: number) {
    this.errorMsg = '';
    
    this.service.deleteLand(id)
      .subscribe(res => {
        if (res == 0) {
          this.ngOnInit();
        } else {
          this.errorMsg = "Error while deleting...";
        }
      });
  }

}
