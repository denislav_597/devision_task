import { Component, OnInit } from '@angular/core';
import { Land } from '../../shared/models/land.model';
import { LandService } from "../../shared/services/land.service";
import { Router } from "@angular/router";
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-land-create',
  templateUrl: './land-create.component.html',
  styleUrls: ['./land-create.component.css']
})
export class LandCreateComponent implements OnInit {
  land: Land = { name: '', agriculture_crop: '', area: 0 };
  errorMsg: string = '';
  
  landForm = new FormGroup({
    name: new FormControl(null, Validators.required),
    agriculture_crop: new FormControl(null, Validators.required),
    area: new FormControl(null, Validators.required),
  });

  constructor(private service: LandService, private router: Router) { }

  ngOnInit() {
  }

  createLand() {
    this.errorMsg = '';

    this.service.createLand(this.land)
      .subscribe(res => {
        if (Number(res) == 0) {
          this.router.navigate(['/lands']);
        } else {
          this.errorMsg = "Error while creating...";
        }
      })
  }

  cancelCreate() {
    this.router.navigate(['/lands']);
  }

}
