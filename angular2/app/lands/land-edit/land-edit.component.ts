import { Component, OnInit } from '@angular/core';
import { Land } from '../../shared/models/land.model';
import { LandService } from "../../shared/services/land.service";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-land-edit',
  templateUrl: './land-edit.component.html',
  styleUrls: ['./land-edit.component.css']
})

export class LandEditComponent implements OnInit {
  land: Land;
  errorMsg: string = '';
    
  landForm = new FormGroup({
    name: new FormControl(null, Validators.required),
    agriculture_crop: new FormControl(null, Validators.required),
    area: new FormControl(null, Validators.required),
  });

  constructor(private service: LandService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    let id = this.route.snapshot.params['id'];
    this.service.getLand(id)
      .subscribe(land => this.land = land);
  }

  updateLand() {
    this.errorMsg = '';

    this.service.updateLand(this.land)
      .subscribe(
        res => {
          if (Number(res) == 0) {
            this.router.navigate(['/lands']);
          } else {
            this.errorMsg = "Error while editing...";
          }
        },
        err => {
          this.errorMsg = "Error while editing...";
        }
      )
  }

  deleteLand(id: number) {
    this.errorMsg = '';

    this.service.deleteLand(id)
      .subscribe(res => {
        if (res == 0) {
          this.router.navigate(['/lands']);
        } else {
          this.errorMsg = "Error while deleting...";
        }
      });
  }

  cancelEdit() {
    this.router.navigate(['/lands']);
  }


}
