import { Component, OnInit } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lands',
  templateUrl: './lands.component.html',
  styleUrls: ['./lands.component.css']
})
export class LandsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    this.checkLoggedIn();
  }

  checkLoggedIn() {
    let loggedin = Cookie.get('loggedin');
    
    if (Number(loggedin) != 1) {
      this.router.navigate(['/login']);
    }
  }

}
