import { Component, OnInit } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { Login } from '../shared/models/login.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  login: Login = { name: '', password: '' };
  errorMsg: string;
  loginForm = new FormGroup({
    name: new FormControl(null, Validators.required),
    password: new FormControl(null, Validators.required)
  });
  
  constructor(private router: Router) { }

  ngOnInit() {
  }

  logIn() {
    this.errorMsg = '';

    if ((this.login.name == 'test') && (this.login.password == 'test')) {
      Cookie.set('loggedin', '1');
    } else {
      Cookie.delete('loggedin');
      this.errorMsg = 'Try again';
    }

    let loggedin = Cookie.get('loggedin');
    if (Number(loggedin) == 1) {
      this.router.navigate(['/lands']);
    }
    
  }

}
