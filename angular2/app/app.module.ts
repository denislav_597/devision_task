import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from "@angular/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { routing } from "./app.routing";

import { LandService } from "./shared/services/land.service";
import { TractorService } from './shared/services/tractor.service';
import { CultivatedLandService } from './shared/services/cultivated-land.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { AppComponent } from './app.component';
import { LandsComponent } from './lands/lands.component';
import { LandListComponent } from './lands/land-list/land-list.component';
import { LandEditComponent } from './lands/land-edit/land-edit.component';
import { LandCreateComponent } from './lands/land-create/land-create.component';
import { TractorsComponent } from './tractors/tractors.component';
import { TractorCreateComponent } from './tractors/tractor-create/tractor-create.component';
import { TractorEditComponent } from './tractors/tractor-edit/tractor-edit.component';
import { TractorListComponent } from './tractors/tractor-list/tractor-list.component';
import { CultivatedLandComponent } from './cultivated-land/cultivated-land.component';
import { CultivatedLandCreateComponent } from './cultivated-land/cultivated-land-create/cultivated-land-create.component';
import { CultivatedLandEditComponent } from './cultivated-land/cultivated-land-edit/cultivated-land-edit.component';
import { CultivatedLandListComponent } from './cultivated-land/cultivated-land-list/cultivated-land-list.component';
import { ReportComponent } from './report/report.component';
import { ReportService } from './shared/services/report.service';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    LandsComponent,
    LandListComponent,
    LandEditComponent,
    LandCreateComponent,
    TractorsComponent,
    TractorCreateComponent,
    TractorEditComponent,
    TractorListComponent,
    CultivatedLandComponent,
    CultivatedLandCreateComponent,
    CultivatedLandEditComponent,
    CultivatedLandListComponent,
    ReportComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    ReactiveFormsModule,
  ],
  providers: [
    LandService,
    TractorService,
    CultivatedLandService,
    ReportService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
