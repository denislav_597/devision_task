import { Component, OnInit } from '@angular/core';
import { Tractor } from '../../shared/models/tractor.model';
import { TractorService } from "../../shared/services/tractor.service";
import * as $ from 'jquery';

@Component({
  selector: 'app-tractor-list',
  templateUrl: './tractor-list.component.html',
  styleUrls: ['./tractor-list.component.css']
})

export class TractorListComponent implements OnInit {
  tractors: Tractor[];
  errorMsg: string = '';
  
  constructor(private service: TractorService) { }

  // Get tractors
  ngOnInit() {
    this.service.getTractors()
      .subscribe(tractors => this.tractors = tractors);  
  }

  // Delete tractor
  deleteTractor(id: number) {
    this.errorMsg = '';
    
    this.service.deleteTractor(id)
      .subscribe(res => {
        if (res == 0) {
          this.ngOnInit();
        } else {
          this.errorMsg = "Error while deleting...";
        }
      });
  }

}
