import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TractorCreateComponent } from './tractor-create.component';

describe('TractorCreateComponent', () => {
  let component: TractorCreateComponent;
  let fixture: ComponentFixture<TractorCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TractorCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TractorCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
