import { Component, OnInit } from '@angular/core';
import { Tractor } from '../../shared/models/tractor.model';
import { TractorService } from "../../shared/services/tractor.service";
import { Router } from "@angular/router";
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-tractor-create',
  templateUrl: './tractor-create.component.html',
  styleUrls: ['./tractor-create.component.css']
})
export class TractorCreateComponent implements OnInit {
  tractor: Tractor = { name: '' };
  errorMsg: string = '';
  
  tractorForm = new FormGroup({
    name: new FormControl(null, Validators.required)
  });

  constructor(private service: TractorService, private router: Router) { }

  ngOnInit() {
  }  

  createTractor() {
    this.errorMsg = '';

    this.service.createTractor(this.tractor)
      .subscribe(res => {
        if (Number(res) == 0) {
          this.router.navigate(['/tractors']);
        } else {
          this.errorMsg = "Error while creating...";
        }
      })
  }

  cancelCreate() {
    this.router.navigate(['/tractors']);
  }

}
