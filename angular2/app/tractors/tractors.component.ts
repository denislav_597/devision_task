import { Component, OnInit } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tractors',
  templateUrl: './tractors.component.html',
  styleUrls: ['./tractors.component.css']
})
export class TractorsComponent implements OnInit {

  constructor(private router: Router) { }
  
  ngOnInit() {
    this.checkLoggedIn();
  }

  checkLoggedIn() {
    let loggedin = Cookie.get('loggedin');
    if (Number(loggedin) != 1) {
      this.router.navigate(['/login']);
    }
  }

}
