import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TractorEditComponent } from './tractor-edit.component';

describe('TractorEditComponent', () => {
  let component: TractorEditComponent;
  let fixture: ComponentFixture<TractorEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TractorEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TractorEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
