import { Component, OnInit } from '@angular/core';
import { Tractor } from "../../shared/models/tractor.model";
import { TractorService } from "../../shared/services/tractor.service";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-tractor-edit',
  templateUrl: './tractor-edit.component.html',
  styleUrls: ['./tractor-edit.component.css']
})
export class TractorEditComponent implements OnInit {
  tractor: Tractor;
  errorMsg: string = '';
  
  tractorForm = new FormGroup({
    name: new FormControl(null, Validators.required)
  });

  constructor(private service: TractorService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    let id = this.route.snapshot.params['id'];
    this.service.getTractor(id)
      .subscribe(tractor => this.tractor = tractor);
  }

  updateTractor() {
    this.errorMsg = '';

    this.service.updateTractor(this.tractor)
      .subscribe(
        res => {
          if (Number(res) == 0) {
            this.router.navigate(['/tractors']);
          } else {
            this.errorMsg = "Error while editing...";
          }
        },
        err => {
          this.errorMsg = "Error while editing...";
        }
      )
  }

  deleteTractor(id: number) {
    this.errorMsg = '';

    this.service.deleteTractor(id)
      .subscribe(res => {
        if (res == 0) {
          this.router.navigate(['/tractors']);
        } else {
          this.errorMsg = "Error while deleting...";
        }
      });
  }

  cancelEdit() {
    this.router.navigate(['/tractors']);
  }


}
