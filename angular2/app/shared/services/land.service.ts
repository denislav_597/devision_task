import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Http, Response, RequestOptions, Headers} from "@angular/http";
import { Land } from '../models/land.model';

@Injectable()
export class LandService {
    private url = 'https://devision-task.000webhostapp.com/web/index.php?r=land/';

    constructor(private http: Http) {
    }

    // Get lands
    getLands(): Observable<Land[]> {
        return this.http.get(this.url+"list-land")
            .map(res => res.json())
            .catch(this.handleError);
    }

    // Get land
    getLand(id: number): Observable<Land> {
        return this.http.get(this.url+"list-land&id="+id)
            .map(res => res.json())
            .catch(this.handleError);
    }

    // Create land
    createLand(land: Land): Observable<Land> {
        return this.http.post(this.url+"create-land", JSON.stringify(land))
            .map(res => res.json())
            .catch(this.handleError);
    }

    // Update land
    updateLand(land: Land): Observable<Land> {
        return this.http.post(this.url+"edit-land/&id="+land.id, JSON.stringify(land))
            .map(res => res.json())
            .catch(this.handleError);
    }

    // Delete land
    deleteLand(id: number) {
        return this.http.get(this.url+"delete-land/&id="+id)
            .map(res => res.json())
            .catch(this.handleError);
    }

    private handleError(err) {
        let errMsg: string;
        
        if (err instanceof Response) {
            let body = err.json() || '';
            let error = body.error || JSON.stringify(body);
            errMsg = `${err.status} - ${err.statusText || ''} ${error}`;
        } else {
            errMsg = err.message ? err.message : err.toString();
        }

        return Observable.throw(errMsg);
    }

}