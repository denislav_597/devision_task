import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Http, Response, RequestOptions, Headers} from "@angular/http";
import { Tractor } from "../models/tractor.model";

@Injectable()
export class TractorService {
    private url = 'https://devision-task.000webhostapp.com/web/index.php?r=tractor/';
    
    constructor(private http: Http) {
    }

    // Get tractors
    getTractors(): Observable<Tractor[]> {
        return this.http.get(this.url+"list-tractors")
            .map(res => res.json())
            .catch(this.handleError);
    }

    // Get tractor
    getTractor(id: number): Observable<Tractor> {
        return this.http.get(this.url+"list-tractors&id="+id)
            .map(res => res.json())
            .catch(this.handleError);
    }

    // Create tractor
    createTractor(tractor: Tractor): Observable<Tractor> {
        return this.http.post(this.url+"create-tractor", JSON.stringify(tractor))
            .map(res => res.json())
            .catch(this.handleError);
    }

    // Update tractor
    updateTractor(tractor: Tractor): Observable<Tractor> {
        return this.http.post(this.url+"edit-tractor/&id="+tractor.id, JSON.stringify(tractor))
            .map(res => res.json())
            .catch(this.handleError);
    }

    // Delete tractor
    deleteTractor(id: number) {
        return this.http.get(this.url+"delete-tractor/&id="+id)
            .map(res => res.json())
            .catch(this.handleError);
    }

    private handleError(err) {
        let errMsg: string;
        
        if (err instanceof Response) {
            let body = err.json() || '';
            let error = body.error || JSON.stringify(body);
            errMsg = `${err.status} - ${err.statusText || ''} ${error}`;
        } else {
            errMsg = err.message ? err.message : err.toString();
        }

        return Observable.throw(errMsg);
    }

}
