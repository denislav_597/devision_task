import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Http, Response, RequestOptions, Headers} from "@angular/http";
import { CultivatedLand } from '../models/cultivated-land.model';
import { Report } from "../models/report.model";

@Injectable()
export class ReportService {
    private url = 'https://devision-task.000webhostapp.com/web/index.php?r=report/';

    constructor(private http: Http) {
    }

    // Get cultivated lands
    getReport(): Observable<Report[]> {
        return this.http.get(this.url+"get-report")
            .map((res:Response) => res.json())
            .catch(this.handleError);
    }

    private handleError(err) {
        let errMsg: string;
        
        if (err instanceof Response) {
            let body = err.json() || '';
            let error = body.error || JSON.stringify(body);
            errMsg = `${err.status} - ${err.statusText || ''} ${error}`;
        } else {
            errMsg = err.message ? err.message : err.toString();
        }

        return Observable.throw(errMsg);
    }

}