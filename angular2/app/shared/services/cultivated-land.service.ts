import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Http, Response, RequestOptions, Headers} from "@angular/http";
import { CultivatedLand } from '../models/cultivated-land.model';

@Injectable()
export class CultivatedLandService {
    private url = 'https://devision-task.000webhostapp.com/web/index.php?r=cultivated-land/';

    constructor(private http: Http) {
    }

    // Get cultivated lands
    getCultivatedLands(): Observable<CultivatedLand[]> {
        return this.http.get(this.url+"list-cultivated-land")
            .map(res => res.json())
            .catch(this.handleError);
    }

    // Get cultivated land
    getCultivatedLand(id: number): Observable<CultivatedLand> {
        return this.http.get(this.url+"list-cultivated-land&id="+id)
            .map(res => res.json())
            .catch(this.handleError);
    }

    // Create cultivated land
    createCultivatedLand(cultivated_land: CultivatedLand): Observable<CultivatedLand> {
        return this.http.post(this.url+"create-cultivated-land", JSON.stringify(cultivated_land))
            .map(res => res.json())
            .catch(this.handleError);
    }

    // Update cultivated land
    updateCultivatedLand(cultivated_land: CultivatedLand): Observable<CultivatedLand> {
        return this.http.post(this.url+"edit-cultivated-land/&id="+cultivated_land.id, JSON.stringify(cultivated_land))
            .map(res => res.json())
            .catch(this.handleError);
    }

    // Delete cultivated land
    deleteCultivatedLand(id: number) {
        return this.http.get(this.url+"delete-cultivated-land/&id="+id)
            .map(res => res.json())
            .catch(this.handleError);
    }

    private handleError(err) {
        let errMsg: string;
        
        if (err instanceof Response) {
            let body = err.json() || '';
            let error = body.error || JSON.stringify(body);
            errMsg = `${err.status} - ${err.statusText || ''} ${error}`;
        } else {
            errMsg = err.message ? err.message : err.toString();
        }

        return Observable.throw(errMsg);
    }

}