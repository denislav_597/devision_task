export class CultivatedLand {
    id?: number;
    tractor_id: number;
    tractor_name?: string;
    land_id: number;
    land_name?: string;
    date: string;
    deleted?: number;
}