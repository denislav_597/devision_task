export class Report {
    sum: number;
    report: {
        id: number;
        land_id: number;
        land_name: string;
        t_name: string;
        tractor_id: number;
        area: number;
        date: number
        deleted: number;        
    }
}