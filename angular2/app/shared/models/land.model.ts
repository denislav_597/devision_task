export class Land {
    id?: number;
    name: string;
    agriculture_crop: string;
    area: number;
    deleted?: number;
}