<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Users extends ActiveRecord {

	public static function tableName() {
		return 'users';
	}


	public function rules() {
		return [
			[['name','password'] ,'required'],
			[['name','password'] ,'string', 'max'=>100]
		];
	}




}