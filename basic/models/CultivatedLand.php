<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class CultivatedLand extends ActiveRecord {

	public static function tableName() {
		return 'cultivated_land';
	}
	
}