<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Tractor;

class TractorController extends Controller {

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    // http://localhost/basic/web/index.php?r=tractor/list-tractors
    public function actionListTractors($id = 0) {
        $id = intval($id);

        if (isset($id) && ($id > 0)) {
            $tractors = Tractor::find()->where(['id'=>$id,'deleted'=>0])->asArray()->one();
        } else {
            $tractors = Tractor::find()->where(['deleted'=>0])->asArray()->all();
        }

        return json_encode($tractors);
    }

    // http://localhost/basic/web/index.php?r=tractor/create-tractor
    public function actionCreateTractor() {
        $model = new Tractor();

        return $this->edit_tractor($model);

        return 3;
    }

    // http://localhost/basic/web/index.php?r=tractor/edit-tractor&id=1
    public function actionEditTractor($id = 0) {
        $id = intval($id);

        $model = Tractor::find()->where(['id'=>$id])->one();
        if ($model == null) {
            return 3;
        }

        return $this->edit_tractor($model);

        return 4;
    }

    // http://localhost/basic/web/index.php?r=tractor/delete-tractor&id=1
    public function actionDeleteTractor($id = 0) {
        $id = intval($id);
        $model = Tractor::find()->where(['id'=>$id])->one();

        if ($model == null) {
            return 1;
        }

        $model->deleted = 1;
        if ($model->save()) {
            return 0;
        }

        return 2;
    }

    public function edit_tractor($model) {
        $data = json_decode(file_get_contents('php://input'), 1);

        if (isset($data['name'])) {
            $model->name = Yii::$app->db->quoteValue($data['name']);
            $model->name = str_replace("'","", $model->name);

            if ($model->save()) {
                return 0;
            }
            return 1;
        }
        return 2;
    }

}