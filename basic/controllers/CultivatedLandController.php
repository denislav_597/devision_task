<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\CultivatedLand;
use yii\base\Exception;

class CultivatedLandController extends Controller {

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    // http://localhost/basic/web/index.php?r=cultivated-land/list-cultivated-land
    public function actionListCultivatedLand($id = 0) {
        $id = intval($id);

        if (isset($id) && ($id > 0)) {
            $cultivated_lands = CultivatedLand::find()->where(['id'=>$id,'deleted'=>0])->asArray()->one();
        } else {
            $cultivated_lands = CultivatedLand::find()->where(['deleted'=>0])->asArray()->all();
        }

        return json_encode($cultivated_lands);
    }

    // http://localhost/basic/web/index.php?r=cultivated-land/create-cultivated-land
    public function actionCreateCultivatedLand() {
        $model = new CultivatedLand();
        return $this->edit_cultivated_land($model);
    }

    // http://localhost/basic/web/index.php?r=cultivated-land/edit-cultivated-land&id=1
    public function actionEditCultivatedLand($id = 0) {
        $id = intval($id);

        $model = CultivatedLand::find()->where(['id'=>$id])->one();
        if ($model == null) {
            return 6;
        }

        return $this->edit_cultivated_land($model);
    }

    // http://localhost/basic/web/index.php?r=cultivated-land/delete-cultivated-land&id=1
    public function actionDeleteCultivatedLand($id = 0) {
        $id = intval($id);
        $model = CultivatedLand::find()->where(['id'=>$id])->one();

        if ($model == null) {
            return 1;
        }

        $model->deleted = 1;
        if ($model->save()) {
            return 0;
        }

        return 2;
    }

    public function edit_cultivated_land($model) {
        $data = json_decode(file_get_contents('php://input'), 1);

        if (isset($data['tractor_id']) && isset($data['land_id']) && isset($data['date']) && isset($data['area'])) {

            $sql_data = array();
            $sql_data['tractor_id'] = intval($data['tractor_id']);
            $sql_data['land_id'] = intval($data['land_id']);
            $sql_data['date'] = Yii::$app->db->quoteValue($data['date']);
            $sql_data['area'] = floatval($data['area']);
            $sql_data['date'] = str_replace("'","", $sql_data['date']);

            // Transaction
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if (isset($model->id) && ($model->id > 0)) {
                    Yii::$app->db->createCommand()->update('cultivated_land',$sql_data,"id = {$model->id}")->execute();
                } else {
                    Yii::$app->db->createCommand()->insert('cultivated_land',$sql_data)->execute();
                }

                // Check for tractor
                $tractor_query = (new \yii\db\Query)
                    ->select('id')
                    ->from('tractors')
                    ->where(['id' => $sql_data['tractor_id'], 'deleted' => 0]);

                if ($tractor_query->count() == 0) {
                    // Tractor not found
                    return 1;
                }

                // Get area for current land
                $land_query = (new \yii\db\Query)
                    ->select('area')
                    ->from('land')
                    ->where(['id' => $sql_data['land_id'], 'deleted' => 0]);

                if ($land_query->count() == 0) {
                    // Area not found
                    return 2;
                } else {
                    $land_area = $land_query->one();
                    $land_area = $land_area['area'];
                }


                // Get sum of areas for current land
                $query_cultivated_land_area = (new \yii\db\Query)
                    ->select('cl.*')
                    ->from('cultivated_land as cl')
                    ->join('join', 'land as l', 'l.id = cl.land_id')
                    ->where(['cl.deleted' => 0])
                    ->andWhere(['l.deleted' => 0])
                    ->andWhere(['l.id' => $sql_data['land_id']]);

                $cultivated_land_area_sum = $query_cultivated_land_area->sum('cl.area');

                if ($land_area < $cultivated_land_area_sum) {
                    throw new Exception ('Land area exceeded');
                } else {
                    $transaction->commit();
                    return 0;
                }

            } catch (\Exception $e) {
                $transaction->rollBack();
                return 3;
            }
            return 4;
        }
        return 5;
    }


}
