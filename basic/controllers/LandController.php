<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Land;

class LandController extends Controller {

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    // http://localhost/basic/web/index.php?r=land/list-land
    public function actionListLand($id = 0) {
        $id = intval($id);

        if (isset($id) && ($id > 0)) {
            $lands = Land::find()->where(['id'=>$id,'deleted'=>0])->asArray()->one();
        } else {
            $lands = Land::find()->where(['deleted'=>0])->asArray()->all();
        }
        return json_encode($lands);
    }

    // http://localhost/basic/web/index.php?r=land/create-land
    public function actionCreateLand() {
        $model = new Land();

        return $this->edit_land($model);

        return 3;
    }

    // http://localhost/basic/web/index.php?r=land/edit-land&id=1
    public function actionEditLand($id = 0) {
        $id = intval($id);

        $model = Land::find()->where(['id'=>$id])->one();
        if ($model == null) {
            return 3;
        }

        return $this->edit_land($model);

        return 4;
    }

    // http://localhost/basic/web/index.php?r=land/delete-land&id=1
    public function actionDeleteLand($id = 0) {
        $id = intval($id);
        $model = Land::find()->where(['id'=>$id])->one();

        if ($model == null) {
            return 1;
        }

        $model->deleted = 1;
        if ($model->save()) {
            return 0;
        }

        return 2;
    }

    public function edit_land($model) {
        $data = json_decode(file_get_contents('php://input'), 1);

        if (isset($data['name']) && isset($data['agriculture_crop']) && isset($data['area'])) {
            $model->name = Yii::$app->db->quoteValue($data['name']);
            $model->agriculture_crop = Yii::$app->db->quoteValue($data['agriculture_crop']);
            $model->area = number_format(floatval($data['area']), 2);

            $model->name = str_replace("'","", $model->name);
            $model->agriculture_crop = str_replace("'","", $model->agriculture_crop);

            if ($model->save()) {
                return 0;
            }
            return 1;
        }
        return 2;
    }

}


?>