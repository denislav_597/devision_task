<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Users;

class UsersController extends Controller {
	
	public function actionIndex() {
		$users = Users::find()->asArray()->all();
		//$users=json_encode($users);
		return $this->render('index',['users'=>$users]);		
	}

	public function actionCreate() {
		$model = new Users();

		// new record
		if ($model->load(Yii::$app->request->post()) && $model->save()){
			return $this->redirect(['index']);
		}

		return $this->render('create',['model'=>$model]);
	}

	public function actionEdit($id) {
		//$model = new Users();
		$model = Users::find()->where(['id'=>$id])->one();

		if ($model == null) {
			throw new NotFoundHttpException('The requested page does not exists');
		}

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['index']);
		}

		return $this->render('edit', ['model'=>$model]);

	}

	public function actionDelete($id) {
		$model = Users::findOne($id);

		// $id not found in db
		if ($model == null) {
			throw new NotFoundHttpException('The requested page does not exists.');
		}

		$model->delete();

		return $this->redirect(['index']);
	}
}