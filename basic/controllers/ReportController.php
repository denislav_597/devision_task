<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Report;

class ReportController extends Controller {

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    // https://devision-task.000webhostapp.com/web/index.php?r=report/get-report
    public function actionGetReport() {
        /*
        $model = new Report();
        $sql = "SELECT cl.* FROM cultivated_land as cl
                JOIN land as l on l.id = cl.land_id
                JOIN tractors as t on t.id = cl.tractor_id
                WHERE cl.deleted = 0 AND l.deleted = 0 AND t.deleted = 0";
        $report = Report::findBySql($sql)->asArray()->all();
        */

        $query = (new \yii\db\Query)
            ->select('cl.*, l.name as land_name, l.agriculture_crop as land_agriculture_crop, t.name as tractor_name')
            ->from('cultivated_land as cl')
            ->join('join', 'land as l', 'l.id = cl.land_id')
            ->join('join', 'tractors as t', 't.id = cl.tractor_id')
            ->where(['cl.deleted' => '0'])
            ->andWhere(['l.deleted' => '0'])
            ->andWhere(['t.deleted' => '0']);

        $report_sum = $query->sum('cl.area');
        $report = $query->all();

        $r['sum'] = $report_sum;
        $r['report'] = $report;
        return json_encode($r);
    }

}