<?php
use yii\helpers\Html;
?>

<style>
	table th,td {
		padding: 10px;
	}
</style>

<?= Html::a('Create', ['users/create'], ['class'=>'btn btn-success']); ?>

<table border='1'>
	<tr>
		<th>Name</th>
		<th>Password</th>
		<th>Action</th>
	</tr>
	<?php foreach ($users as $k => $v) { ?>
		<tr>
			<td><?= $v['name'] ?> </td>
			<td><?= $v['password'] ?> </td>
			<td><?= Html::a('Edit', ['users/edit', 'id'=>$v['id']]) ?> | <?= Html::a('Delete', ['users/delete', 'id'=>$v['id']]) ?> </td>
		</tr>
	<?php } ?>
</table>

